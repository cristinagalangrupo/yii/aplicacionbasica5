<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Trabajadores;
use app\models\Delegacion;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionConsulta12() {

        return $this->render("consulta12", [
                    "modelos" => Trabajadores::find()->all(),
        ]);
    }

    public function actionConsulta14() {
        /* active record */
        $a = Delegacion::find()->all();
        /* create command */
        $b = Yii::$app
                ->db
                ->createCommand("select * from delegacion")
                ->queryAll();
        /* Query builder DAO */
        // no tiene sentido que haga esto porque tengo active record
        $c = (new \yii\db\Query())
                ->select(['*'])
                ->from('delegacion')
                ->all();
// con queryBuilder 

        return $this->render("consulta14", [
                    "uno" => $a,
                    "dos" => $b,
                    'tres' => $c
        ]);
    }

    public function actionConsulta17() {
        $a = Delegacion::find()
                ->where(['poblacion' => "Santander"])
                ->all();
        $b = Yii::$app
                ->db
                ->createCommand('select * from delegacion where poblacion="santander"')
                ->queryAll();

        $c = Trabajadores::find()
                ->where(['delegacion' => 1])
                ->all();
        $d = Yii::$app
                ->db
                ->createCommand('select * from trabajadores where delegacion=1')
                ->queryAll();

        $e = Trabajadores::find()
                ->orderby(['nombre' => SORT_ASC])
                ->all();
        $f = Yii::$app->db
                ->createCommand('select * from trabajadores order by nombre')
                ->queryAll();

        $g = Trabajadores::find()
                ->where(['fechaNacimiento' => "null"])
                ->all();
        $h = Yii::$app->db
                ->createCommand('select * from trabajadores where fechaNacimiento="null"')
                ->queryAll();

        return $this->render("consulta17", [
                    "resultados" => [$a, $b, $c, $d, $e, $f, $g, $h],
        ]);
    }
    
    public function actionConsulta20(){
        $datos=[];
        /* consulta a */
        $datos[]= Delegacion::find()
                ->where(['<>','poblacion','Santander'])
                ->andWhere(['IS NOT','direccion',null])
                ->all();
        
        /* consulta b */
        $datos[]= Trabajadores::find()
                ->joinWith('delegacion0') //para filtrar
                ->where(["poblacion"=>"Santander"])
                ->all();
        
        // OPTIMIZADA
        $a= Delegacion::find()
                ->select('id')
                ->where(["poblacion"=>"Santander"])
                ->asArray()
                ->all();
        
        $a=ArrayHelper::map($a,"id","id");
        
       $datos[]= Trabajadores::find()
                ->where(["in","delegacion",$a])
                ->all();
        
        /* consulta c*/
        
        $datos[]= Trabajadores::find()
                ->with('delegacion0') //si no quiero filtrar, que me saque todo
                ->where("foto is not null")
                ->all();
       
      /* consulta d*/
      
      $datos[]= Delegacion::find()
              ->joinWith("trabajadores t") //por defecto te hace left join
              ->where(["t.id"=>null])
              ->all();
      $delegaciones= Delegacion::find()
              ->with("trabajadores")
              ->asArray()
              ->all();
      
      foreach ($delegaciones as $delegacion){
          var_dump(count($delegacion["trabajadores"]));
      }
     /*
      $prueba[]= Delegacion::find()
              ->with("trabajadores t") //por defecto te hace left join
              ->where(["t.delegacion"=> "is not null"])
              ->all();
                */
    //$prueba1=Trabajadores::find()->all()[0]->delegacion0;        
        return $this->render("consulta20",[
            "resultado"=>$datos,
          
        ]);
    }
    public function actionConsulta22(){
       $ncompleto= Trabajadores::getConsulta4();
       // $nCompleto= Trabajadores::consulta1_2();
        return $this->render("consulta22",[
            "ncompleto"=>$ncompleto
        ]);
    }
}
