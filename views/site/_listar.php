<?php
use yii\helpers\Html;
//var_dump($dato[0]);
?>

<div class="panel panel-primary">
  <div class="panel-heading"><?= $dato->id ?></div>
  <div class="panel-body">
   Nombre: <?= $dato->nombre ?>
  </div>
  <div class="panel-body">
   Apellido: <?= $dato->apellidos ?>
  </div>
  <div class="row row-flex row-flex-wrap">
      <div class="col-xs-3 col-md-3 pull-right flex-col">
      <figure>
        <?= Html::img('@web/imgs/'.$dato->foto,[
      "class"=>"img-responsive",
      "width"=>"200px"]) ?>
  </figure>
  </div>
  </div>
  
</div>

