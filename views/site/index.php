<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>CONSULTAS</h1>

        <ul class="nav nav-pills nav-stacked">
 
  <li role="presentation"><?= Html::a('Consulta 12', ['site/consulta12',], ['class' => 'btn btn-success','style'=>'font-size:30px']) ?></li>
  <li role="presentation"><?= Html::a('Consulta 14', ['site/consulta14',], ['class' => 'btn btn-success','style'=>'font-size:30px;']) ?></li>
  <li role="presentation"><?= Html::a('Consulta 17', ['site/consulta17',], ['class' => 'btn btn-success','style'=>'font-size:30px;']) ?></li>
  <li role="presentation"><?= Html::a('Consulta 20', ['site/consulta20',], ['class' => 'btn btn-success','style'=>'font-size:30px;']) ?></li>
  <li role="presentation"><?= Html::a('Consulta 22', ['site/consulta22',], ['class' => 'btn btn-success','style'=>'font-size:30px;']) ?></li>
</ul>

    </div>

    

    </div>

