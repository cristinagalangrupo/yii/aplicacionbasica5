<?php

use yii\helpers\Html;

//var_dump($dato[0]);
foreach ($modelos as $dato) {
    ?>

    <div class="panel panel-primary">
        <div class="panel-heading"><?= $dato->id ?></div>
        <div class="panel-body">
            <p><h3>Nombre: <?= $dato->nombre ?></h3></p>
            <p><h3> Apellido: <?= $dato->apellidos ?></h3></p>
            <div class="row row-flex-wrap">
                <div class="col-xs-6 col-md-3 pull-right flex-col">
                    <figure>
                        <?= Html::img('@web/imgs/' . $dato->foto, ['class' => "img-responsive"]) ?>
                    </figure>
                </div>
            </div>

        </div>
    </div>
    <?php
}
?>
